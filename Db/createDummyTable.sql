USE [jim1]
GO

/****** Object:  Table [dbo].[PARCEL_LOC_LOG]    Script Date: 8/6/2020 11:24:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PARCEL_LOC_LOG](
	[barcode] [nchar](32) NULL,
	[location] [nchar](10) NULL,
	[parcelKey] [nchar](16) NOT NULL,
	[createTS] [datetime] NULL,
	[action] [nchar](2) NULL,
 CONSTRAINT [PK_PARCEL_LOC_LOG] PRIMARY KEY CLUSTERED 
(
	[parcelKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

