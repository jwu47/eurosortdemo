# README #

This the repo for EuroSort and PB parcel systems integration

### What is this repository for? ###

* PoC EuroSort, and later Monroe production
* Project summary - https://confluence.tacitknowledge.com/display/NGSA/Eurosort+Design

### How do I get set up? ###

* Check out the EuroSim.exe from repo (/EuroSortCode/Simulator). This can be run as a standalone simulator. The configuration is self-explanatory.
* Check out the C# project in PoC. It runs as a console app. The main methodis heavily documented.  It needs the SorterAPI.dll by EuroSort, which is in the repo (/EuroSortCode/SorterIntegrationSDK)
* Dependencies. A local MS SQL database.  In produciton this will be replaced by the RabbitMQ.
* Database configuration
A dummy table neded, and script is in /Db
* How to run tests
Start the Simulator, and Run the PoC, then start the data flow by clicking the 'Start' button on Simulator
* Deployment instructions
Currently thhe code only works on a single machhine, where Simulator and PoC communiates via namedPipe.  It can be configured to run on different machines via TCP/IP.

### Who do I talk to? ###

* Jim Wu (jianwei.wu@pb.com)
* Toby Fausett (toby.fausett@pb.com)