﻿using System;
using System.Threading;
using System.Net.Http;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text;

using SORTapi;
using Serilog.Core;
using Serilog;
using Newtonsoft.Json;

// multi-barcode, delimited...

namespace EuroSortSimConsole
{
    /**
    *   This is a console program to demo the integration with EusoSort Simulator.
    *   
    *   Because it's a demo - I made it a simple main() to drive everything. Of course, we 
    *   should break it into classes (patterns) in implementation. 
    *   
    *   The main() func sprawn off 2 threads.
    *   
    *   The main thread - 
    *      1/ heartbeat to check liviness of the simulator;
    *      2/ handle 'GET' event, where EuroSort asks for drop location of a barcode
    *           we actaully make async calls to a mock API on the Internet as if calling
    *           oneShip API for sort map
    *      3/ handle 'VERIFY' event, where EuroSort sends in the parcel dropped at a location event.
    *           we persisit the locaiton and parcel information in a SQL Server table
    *           
    *   The REST thread - (http://locahost:8888/settings, PUT call)  
    *      4/ handle 'SCAN offline' event, where operators scan a location to take the location offline 
    *           and potentially make containerliztion call
    *      5/ handle 'SCAN online' event, where operators scan a locatio nto make it online.
    *   
    *   Use case which is not tied to EuroSort - 
    *      6/ write to Rabbit one message at a time for VERIFY
    *              
    *      7/ Event streaming the 'containerlization' event. This is in-scope work, but we should leverage
    *          what team has built in other projects.
    *        
    **/
    class Program
    {
        public static EuroSort Sorter = new EuroSort("");

        public static bool keepRunning = true;
        public static SqlConnection sqlConn = null;

        public static String dbConnStr = "Server=localhost\\MSSQLSERVER01;Database=jim1;" +
                "Integrated Security=true;Min Pool Size = 5;Max Pool Size=10;User Id=CORPDOM1\\JWu;Password=53Olddog#";

        //handle http SCAN request
        private static Task _httpLoop ;
        // private const int Port = 8888;


        static void Main(string[] args)
        {
            Logger log = new LoggerConfiguration().WriteTo.File("log.txt").CreateLogger();


            //sql server set up
            sqlConn = new SqlConnection(dbConnStr);
            sqlConn.Open();

            //event handler set up
            Sorter.DataArrived += Sorter_DataArrivedAsync;

            Console.WriteLine("===== Start Integration Service =====");

            //start the http loop
            _httpLoop = MainLoop(Sorter);

            Random random = new Random();
            while (keepRunning)
            {
                //breahter against tight-loop 
                Thread.Sleep(random.Next(4000, 12000));

                // heart-beat and logging
                EuroSort.SorterStatus sorterstatus = Sorter.SorterConnection;
                Console.WriteLine(" ! heartbeat to check EuroSor Status: " + sorterstatus + " ! ");

                if (sorterstatus == EuroSort.SorterStatus.Offline)
                    log.Warning(" EuroSort status is Offline");
                if (sorterstatus == EuroSort.SorterStatus.Error)
                    log.Error(" EuroSort status is Error!");

            }

            try
            {
                sqlConn.Close();
                Sorter.Dispose();
                System.Environment.Exit(0);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                System.Environment.Exit(1);

            }
        }

        static async void Sorter_DataArrivedAsync(object sender, EuroSort.SorterData e)
        {
            EuroSort.SorterData MessageOut = new EuroSort.SorterData();

            switch (e.Type)
            {
                case EuroSort.MessageType.REQUEST:
                   
                    Console.WriteLine(" <-- Sorter  REQUEST location for barcode " + e.Barcode);
                    
                    //mock call to get Sort plan, < 200ms.
                    int destination = await CallMockAPI(e.Barcode);
                   
                    MessageOut.Type = EuroSort.MessageType.REQUEST;
                    MessageOut.Barcode = e.Barcode;
                    MessageOut.Destination = destination;
                    MessageOut.TrayID = e.TrayID;

                    Sorter.SendData(MessageOut);
                    Console.WriteLine(" --> Send out destination for barcode " + e.Barcode + ", destination is " + destination);
                    break;
                case EuroSort.MessageType.VERIFY:
                    Console.WriteLine(" <-- Sorter VERIFY for barcode " + e.Barcode
                         + " dropped @ location " + e.Destination);
                    //sql persist
                    PersistVerify(e.Barcode, e.Destination);
                    break;

                case EuroSort.MessageType.SCAN:
                    //handle overriding.
                    //here need to handle exceptions where Sorter can't trun a desitination online
                    Console.WriteLine(" <-- Sorter echoes Status for destination " + e.Destination + " Status is  " + e.DestinationStatus);
                    break;
            }
        }
        
        static void PersistVerify(string barcode, int destination)
        {
            SqlCommand command = new SqlCommand(null, sqlConn);

            // Create and prepare an SQL statement.
            command.CommandText =
                "INSERT INTO dbo.PARCEL_LOC_LOG (parcelKey, barcode, location, createTS, action) " +
                "VALUES (@id, @barcode, @location, @time, @action) ";
            SqlParameter keyParam = new SqlParameter("@id", SqlDbType.NVarChar, 10);
            SqlParameter barcodeParam =
                new SqlParameter("@barcode", SqlDbType.NVarChar, 32);
            SqlParameter locationParam =
              new SqlParameter("@location", SqlDbType.NVarChar, 10);
            SqlParameter timeParm =
                new SqlParameter("@time", System.Data.SqlDbType.DateTime);
            SqlParameter actionParm =
               new SqlParameter("@action", SqlDbType.NVarChar, 2);

            command.Parameters.Add(keyParam);
            command.Parameters.Add(barcodeParam);
            command.Parameters.Add(locationParam);
            command.Parameters.Add(timeParm);
            command.Parameters.Add(actionParm);

            // Call Prepare after setting the Commandtext and Parameters.
            command.Prepare();

            //make a random parcelKey
            Random random = new Random();

            keyParam.Value = random.Next(1000000, 90000000).ToString();   //random parcel key for demo
            barcodeParam.Value = barcode;
            locationParam.Value = destination.ToString();
            timeParm.Value = DateTime.Now;
            actionParm.Value = "A"; //hard coded foradd

            command.ExecuteNonQuery();

            Console.WriteLine("Saved to SQLServer barcode " + barcode +
                " @destination " + destination);
        }

        //mock to call the sort map API...  Async to avoid blocking..
        async static Task<int> CallMockAPI(String barcode)
        {
           using (HttpClient client = new HttpClient())
             {
                 String photoJson = await client.GetStringAsync("https://broken-wood-33.getsandbox.com/users/112");
                 int destination = JsonConvert.DeserializeObject<int>(photoJson);
                 Console.WriteLine("<-- REST callback of destination for barcode " + barcode +
                         " is at location " + destination);


                 return destination;
             }
             
          //  return 500;
        }



        private static async Task MainLoop(EuroSort sorter)
        {
            HttpListener Listener = new HttpListener { Prefixes = { "http://localhost:8888/" } };
            Listener.Start();
            Console.WriteLine("===== HTTP Listener Started ===== ");
            Console.WriteLine(" ");

            while (true)
            {
                try
                {
                    //GetContextAsync() returns when a new request come in
                    var context = await Listener.GetContextAsync();
                    lock (Listener)
                    {
                        if (true) ProcessRequest(context);
                    }
                }
                catch (Exception e)
                {
                    if (e is HttpListenerException) return; //this gets thrown when the listener is stopped
                                                            //TODO: Log the exception
                }
            }
        }
        private static void ProcessRequest(HttpListenerContext context)
        {
            using (var response = context.Response)
            {
                try
                {
                    var handled = false;
                    switch (context.Request.Url.AbsolutePath)
                    {
                        //This is where we do different things depending on the URL
                        //TODO: Add cases for each URL we want to respond to
                        case "/settings":
                            switch (context.Request.HttpMethod)
                            {
                                case "GET":
                                    Console.WriteLine("HTTP Get call received" );

                                    //Get the current settings
                                    response.ContentType = "application/json";

                                    //This is what we want to send back
                                    var responseBody = JsonConvert.SerializeObject("OK");
                                    


                                    //Write it to the response stream
                                    var buffer = Encoding.UTF8.GetBytes(responseBody);
                                    response.ContentLength64 = buffer.Length;
                                    response.OutputStream.Write(buffer, 0, buffer.Length);
                                    handled = true;
                                    break;

                                case "POST":
                                    //Update the settings
                                    using (var body = context.Request.InputStream)
                                    using (var reader = new StreamReader(body, context.Request.ContentEncoding))
                                    {
                                        //Get the data that was sent to us
                                        var json = reader.ReadToEnd();

                                        //Use it to update our settings
                                        //String request = JsonConvert.DeserializeObject<String>(json);
                                        String request = (String)json;
                                    

                                        EuroSort.SorterData scanMessage = new EuroSort.SorterData();

                                        scanMessage.Type = EuroSort.MessageType.SCAN;
                                        scanMessage.Destination = 999;
                                        scanMessage.DestinationStatus = EuroSort.StationStatus.NotReady;

                                        Sorter.SendData(scanMessage);

                                        Console.WriteLine(" ---> Send Message to Sorter to take destination offline: " + request);
                                        //Return 204 No Content to say we did it successfully
                                        response.StatusCode = 204;
                                        handled = true;
                                    }
                                    break;
                            }
                            break;
                    }
                    if (!handled)
                    {
                        response.StatusCode = 404;
                    }
                }
                catch (Exception e)
                {
                    //Return the exception details the client - you may or may not want to do this
                    response.StatusCode = 500;
                    response.ContentType = "application/json";
                    var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(e));
                    response.ContentLength64 = buffer.Length;
                    response.OutputStream.Write(buffer, 0, buffer.Length);

                    //TODO: Log the exception
                }
            }
        }
    }

}
